package be.qualiti.thread;

public class TesteThread {
	
	public static void main(String[] args) {
		
		Contador contador = new Contador();
		Mensageiro mensageiro = new Mensageiro();
		
		contador.start();
		mensageiro.start();
		
	}

}
