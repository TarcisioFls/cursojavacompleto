package br.qualiti.cooperacao;


public class CaixaMensagem {
	
	private String mensagem;

	public synchronized void lerMensagem(String nomeThread) {
		
		if(mensagem != null) {
			System.out.println("Thread Leitor " + nomeThread + ": Mensagem: " + mensagem);
			mensagem = null;
			notifyAll();
		}
		else {
			while ( mensagem == null) {
				try {
					wait();
				}
				catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		}
		
	}
	
	public synchronized void escreverMensagem(String nomeThread, String mensagem) {
		
		if(this.mensagem == null) {
			System.out.println("Thread escritor " + nomeThread + ": Mensagem: " + mensagem);
			this.mensagem = mensagem;
			notifyAll();
		}
		else {
			while (this.mensagem != null) {
				try {
					wait();
				}
				catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		}
		
	}
	
	public String getMensagem() {
		return mensagem;
	}

	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}
}
