package br.qualiti.cooperacao;

public class Escritor extends Thread{
	
	private CaixaMensagem  caixa;
	private String nomeThread;
	private String mensagem;
	
	public Escritor(CaixaMensagem caixa, String  nome, String mensagem) {
		this.caixa = caixa;
		this.nomeThread = nome;
		this.mensagem = mensagem;
	}
	
	public void run() {
		
		for(int i = 0; i < 500; i++) {
			caixa.escreverMensagem(nomeThread, mensagem);
			try {
				Thread.sleep(500);
			}
			catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		
	}

}
