package br.qualiti.cooperacao;

public class Leitor extends Thread{
	
	private CaixaMensagem caixa;
	private String nome;
	
	public Leitor(CaixaMensagem caixa, String nome) {
		this.caixa = caixa;
		this.nome = nome;
	}
	
	public void run() {
		
		for(int i = 0; i < 500; i++){
			
			caixa.lerMensagem(nome);
			try{
				Thread.sleep(500);
			}
			catch (InterruptedException e) {
				e.printStackTrace();
			}
			
		}
		
	}

}
