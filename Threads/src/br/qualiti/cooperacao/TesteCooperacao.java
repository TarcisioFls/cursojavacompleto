package br.qualiti.cooperacao;

public class TesteCooperacao {
	
	public static void main(String[] args) {
		
		CaixaMensagem caixa = new CaixaMensagem();
		
		Escritor esc1 = new Escritor(caixa, "Esc1", "Bom dia");
		Escritor esc2 = new Escritor(caixa, "Esc2", "Boa tarde");
		
		Leitor leitor1 = new Leitor(caixa, "leitorr1");
		Leitor leitor2 = new Leitor(caixa, "leitorr2");
		
		leitor1.start();
		leitor2.start();
		
		esc1.start();
		esc2.start();
		
	}

}
