package br.qualiti.runnable;

public class TesteRunnable {
	
	public static void main(String[] args) {
		
		Contador contador = new Contador();
		Mensageiro mensageiro = new Mensageiro();
		
		Thread t1 = new Thread(contador);
		Thread t2 = new Thread(mensageiro);
		
		t1.start();
		t2.start();
		
		Thread t3 = new Thread ( new Runnable() {
			
			@Override
			public void run() {
				// TODO Auto-generated method stub
				for(int i = 1; i <= 500; i++) {
					System.out.println("Thread " + i);
					try {
						Thread.sleep(500);
					} catch (Exception e) {
						// TODO: handle exception
						e.printStackTrace();
					}
				}
			}
		});
		
		t3.start();
		
	}

}
