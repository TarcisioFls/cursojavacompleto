import java.util.ArrayList;
import java.util.List;
import java.util.Map;


public class Cesta<T extends Fruta, V>{
	
	private List<T> frutas;
	private Map<T, V> map;
	
	public Cesta() {
		frutas = new ArrayList<>();
	}
	
	public double calcularImposto() {
		if(!frutas.isEmpty()) {
			T fruta = frutas.get(0);
			double valor = frutas.size()*fruta.getImposto();
			return valor;
		}
		return 0;
	}
	
	public double calcularValor() {
		if (!frutas.isEmpty()) {
			T fruta = frutas.get(0);
			double valor = frutas.size()*fruta.getValor();
			return valor;
		}
		return 0;
	}

	public List<T> getFrutas() {
		return frutas;
	}

	public void setFrutas(List<T> frutas) {
		this.frutas = frutas;
	}
	
	public static void main(String[] args) {
		Banana b = new Banana();
		Laranja l = new Laranja();
		Cesta<Banana, Laranja> cestaBanana = new Cesta<Banana, Laranja>();
		cestaBanana.getFrutas().add(b);
		
		
	}

}
