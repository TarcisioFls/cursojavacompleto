package br.com.qualiti.sge.modelo;

public class Estudante extends Pessoal{
	
	private Curso curso;
	private int periodo;
	private InstituicaoEnsino escola;
	private TipoNivel nivel;
	private String curriculo;
	
	
	public Curso getCurso() {
		return curso;
	}
	public void setCurso(Curso curso) {
		this.curso = curso;
	}
	public int getPeriodo() {
		return periodo;
	}
	public void setPeriodo(int periodo) {
		this.periodo = periodo;
	}
	public InstituicaoEnsino getEscola() {
		return escola;
	}
	public void setEscola(InstituicaoEnsino escola) {
		this.escola = escola;
	}
	public TipoNivel getNivel() {
		return nivel;
	}
	public void setNivel(TipoNivel nivel) {
		this.nivel = nivel;
	}
	public String getCurriculo() {
		return curriculo;
	}
	public void setCurriculo(String curriculo) {
		this.curriculo = curriculo;
	}

}
