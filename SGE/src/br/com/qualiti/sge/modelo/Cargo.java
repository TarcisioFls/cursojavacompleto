package br.com.qualiti.sge.modelo;

public class Cargo {
	
	private Long idCargo;
	private String nome;
	
	
	public Long getIdCargo() {
		return idCargo;
	}
	public void setIdCargo(Long idCargo) {
		this.idCargo = idCargo;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}

}
