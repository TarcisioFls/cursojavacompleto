package br.com.qualiti.sge.modelo;

public class InstituicaoEnsino extends PessoaJuridica{

	private TipoInstituicao natureza;
	private Estudante alunos[];
	private Curso cursos[];
	
	
	public TipoInstituicao getNatureza() {
		return natureza;
	}
	public void setNatureza(TipoInstituicao natureza) {
		this.natureza = natureza;
	}
	public Estudante[] getAlunos() {
		return alunos;
	}
	public void setAlunos(Estudante[] alunos) {
		this.alunos = alunos;
	}
	public Curso[] getCursos() {
		return cursos;
	}
	public void setCursos(Curso[] cursos) {
		this.cursos = cursos;
	}
	
}
