package server;

import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.RMISecurityManager;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;

public class MainServer {
	
	public static void main(String[] args) {
		
		System.setSecurityManager(new RMISecurityManager());
		try {
			LocateRegistry.createRegistry(2120);
			
			IMensagemServer server = new MensagemServerImp();
			
			Naming.rebind("//localhost:2120/mensagemServer", server);
			
			while(true) {
				
			}
		} catch (RemoteException e) {
			e.printStackTrace();
		} catch (MalformedURLException e) {
			e.printStackTrace();
		}
		
	}

}
