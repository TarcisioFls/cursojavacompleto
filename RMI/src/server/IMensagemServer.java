package server;

import java.rmi.Remote;
import java.rmi.RemoteException;

//Passo 1 - Definir interface remota e todos os metodos devem lançarRemoteException
public interface IMensagemServer extends Remote{
	
	void escreverMensagem(String name, String mensagem) throws RemoteException;

}
