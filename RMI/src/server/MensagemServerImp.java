package server;

import java.io.Serializable;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;

/*
 * Passo 2 Criar a classe que implementa a interface remota
 * a classe deve herdar de UnicasRemoteObjetc e implementar e interface remota
 * o construtor deve lançar RemoteException
 */
public class MensagemServerImp extends UnicastRemoteObject implements IMensagemServer, Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public MensagemServerImp() throws RemoteException {
		
	}
	
	@Override
	public void escreverMensagem(String name, String mensagem) throws RemoteException {
		System.out.println(name+" : " + mensagem);
	}

}
