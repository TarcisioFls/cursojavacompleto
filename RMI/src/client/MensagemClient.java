package client;

import java.rmi.RemoteException;

import server.IMensagemServer;

public class MensagemClient {
	
	private String nome;
	private IMensagemServer server;
	private String mensagem;
	
	public MensagemClient(String nome, IMensagemServer server, String mensagem) {
		this.nome = nome;
		this.server = server;
		this.mensagem = mensagem;
	}
	
	public void escreverNoServidor() {
		try {
			server.escreverMensagem(nome, mensagem);
		} catch (RemoteException e) {
			e.printStackTrace();
		}
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public IMensagemServer getServer() {
		return server;
	}

	public void setServer(IMensagemServer server) {
		this.server = server;
	}

	public String getMensagem() {
		return mensagem;
	}

	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}

}
