package client;

import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

import server.IMensagemServer;

public class MainClient {

	public static void main(String[] args) {

		Registry r;
		try {
			r = LocateRegistry.getRegistry("localhost", 2120);
			IMensagemServer server = (IMensagemServer) r.lookup("mensagemServer");
			
			MensagemClient cliente = new MensagemClient("Maria", server, "Aula Final");
			
			cliente.escreverNoServidor();
		} catch (RemoteException e) {
			e.printStackTrace();
		} catch (NotBoundException e) {
			e.printStackTrace();
		}
		
	}

}
