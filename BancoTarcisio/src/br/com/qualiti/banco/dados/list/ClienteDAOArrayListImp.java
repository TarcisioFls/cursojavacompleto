package br.com.qualiti.banco.dados.list;

import java.util.Collections;

import br.com.qualiti.banco.dados.IClienteDAO;
import br.com.qualiti.banco.modelo.Cliente;

public class ClienteDAOArrayListImp extends GenericDAOArrayListImp<Cliente, String> implements IClienteDAO{
	
	@Override
	public Cliente[] listarClientesOrdemCpf() {
		// TODO Auto-generated method stub
		
		if (!repositorio.isEmpty()) {
			Collections.sort(repositorio);
			
			Cliente[] result = new Cliente[repositorio.size()];
			repositorio.toArray(result);
			return result;
		}
		
		return null;
	}

}
