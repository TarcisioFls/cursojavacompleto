package br.com.qualiti.banco.dados.list;

import java.util.ArrayList;
import java.util.List;

import br.com.qualiti.banco.dados.IGenericDAO;
import br.com.qualiti.banco.excecoes.BancoException;
import br.com.qualiti.banco.modelo.AbstractEntify;

public class GenericDAOArrayListImp<T extends AbstractEntify<ID>, ID> implements IGenericDAO<T, ID> {

	protected List<T> repositorio;
	
	public GenericDAOArrayListImp() {
		repositorio = new ArrayList<T>();
	}
	
	@Override
	public void inserir(T entity) throws BancoException {
		
		repositorio.add(entity);
		
	}

	@Override
	public T procurar(ID id) {
		for( T entity: repositorio) {
			if(entity.getID().equals(id)) {
				return entity;
			}
		}
		return null;
	}

	@Override
	public void atualizar(T entity) {
		
		int index = repositorio.indexOf(entity);
		if (index != -1) {
			repositorio.set(index, entity);
		}
		
	}

	@Override
	public void remover(ID id) {
		
		T entity = procurar(id);
		if (entity != null) {
			repositorio.remove(entity);
		}
		
	}

}
