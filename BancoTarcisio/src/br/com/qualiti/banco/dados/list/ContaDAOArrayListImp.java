package br.com.qualiti.banco.dados.list;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import br.com.qualiti.banco.dados.IContaDAO;
import br.com.qualiti.banco.modelo.Cliente;
import br.com.qualiti.banco.modelo.ContaAbstrata;
import br.com.qualiti.banco.modelo.ContaBonus;
import br.com.qualiti.banco.modelo.Poupanca;

public class ContaDAOArrayListImp extends GenericDAOArrayListImp<ContaAbstrata, String>  implements IContaDAO{
	
	
	@Override
	public void renderJurosPoucancas(double taxa) {
		// TODO Auto-generated method stub
		for (ContaAbstrata conta : repositorio) {
			if(conta != null && conta instanceof Poupanca){
				Poupanca p = (Poupanca)conta;
				p.renderJuros(taxa);
			}
		}
		
	}

	@Override
	public void renderBonusContaBonus() {
		// TODO Auto-generated method stub
		for (ContaAbstrata conta : repositorio) {
			if(conta != null && conta instanceof ContaBonus){
				ContaBonus cb = (ContaBonus)conta;
				cb.renderBonus();
			}
		}
		
	}

	@Override
	public ContaAbstrata[] listarContasOrdemSaldo() {
		// TODO Auto-generated method stub
		if (!repositorio.isEmpty()) {		
			Collections.sort(repositorio);
			Collections.reverse(repositorio); //Ordena de forma reversa
			
			ContaAbstrata[] result = new ContaAbstrata[repositorio.size()];
			repositorio.toArray(result);
			return result;
		}
		return null;
	}

	@Override
	public Cliente[] listarClientesSaldoAcimaValor(double valor) {
		// TODO Auto-generated method stub
		
		Set<Cliente> clientesVip = new HashSet<Cliente>();
		for(ContaAbstrata conta : repositorio) {
			if (conta != null && conta.getSaldo() >= valor) {
				clientesVip.add(conta.getCliente());
			}
		}
		
		if(clientesVip.size() > 0) {
			Cliente[] clientes = new Cliente[clientesVip.size()];
			clientesVip.toArray(clientes);
			return clientes;
		}
		
		return null;
	}

	@Override
	public String imprimirExtrato(String numero) {
		// TODO Auto-generated method stub
		
		
		
		return null;
	}

}
