package br.com.qualiti.banco.dados;

import br.com.qualiti.banco.modelo.Endereco;

public interface IEnderecoDAO extends IGenericDAO<Endereco, Long>{

}
