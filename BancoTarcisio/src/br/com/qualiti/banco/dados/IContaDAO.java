package br.com.qualiti.banco.dados;

import br.com.qualiti.banco.modelo.Cliente;
import br.com.qualiti.banco.modelo.ContaAbstrata;

public interface IContaDAO extends IGenericDAO<ContaAbstrata, String>{
	
	void renderJurosPoucancas(double taxa);
	void renderBonusContaBonus();
	ContaAbstrata[] listarContasOrdemSaldo();
	Cliente[] listarClientesSaldoAcimaValor(double valor);
	String imprimirExtrato(String numero);

}
