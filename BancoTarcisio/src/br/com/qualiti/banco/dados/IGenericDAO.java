package br.com.qualiti.banco.dados;

import br.com.qualiti.banco.excecoes.BancoException;

public interface IGenericDAO<T, ID> {
	
	void inserir(T entity) throws BancoException;
	T procurar (ID id);
	void atualizar(T entity);
	void remover(ID id) throws BancoException;

}
