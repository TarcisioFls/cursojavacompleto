package br.com.qualiti.banco.dados.map;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import br.com.qualiti.banco.dados.IContaDAO;
import br.com.qualiti.banco.excecoes.BancoException;
import br.com.qualiti.banco.modelo.Cliente;
import br.com.qualiti.banco.modelo.ContaAbstrata;
import br.com.qualiti.banco.modelo.ContaBonus;
import br.com.qualiti.banco.modelo.Movimentacao;
import br.com.qualiti.banco.modelo.Poupanca;

public class ContaDAOMapImp implements IContaDAO{
	
	private Map<String, ContaAbstrata> repositorio;
	
	public ContaDAOMapImp() {
		
		repositorio = new HashMap<String, ContaAbstrata>();
		
	}

	@Override
	public void inserir(ContaAbstrata conta) throws BancoException {
		
		repositorio.put(conta.getNumero(), conta);
		
	}

	@Override
	public ContaAbstrata procurar(String numero) {
		
		return repositorio.get(numero);
		
	}

	@Override
	public void atualizar(ContaAbstrata conta) {
		
		repositorio.put(conta.getNumero(), conta);
		
	}

	@Override
	public void remover(String numero) {
		
		repositorio.remove(numero);
		
	}

	@Override
	public void renderJurosPoucancas(double taxa) {
		
		Collection<ContaAbstrata> contas = repositorio.values();
		for (ContaAbstrata conta : contas) {
			if (conta instanceof Poupanca) {
				Poupanca p = (Poupanca)conta;
				p.renderJuros(taxa);
			}
		}
		
	}

	@Override
	public void renderBonusContaBonus() {
		
		Collection<ContaAbstrata> contas = repositorio.values();
		for (ContaAbstrata conta : contas) {
			if (conta instanceof ContaBonus) {
				ContaBonus p = (ContaBonus)conta;
				p.renderBonus();
			}
		}
		
	}

	@Override
	public ContaAbstrata[] listarContasOrdemSaldo() {
		
		Collection<ContaAbstrata> contas = repositorio.values();
		List<ContaAbstrata> listaContas = new ArrayList<ContaAbstrata>(contas);
		
		Collections.sort(listaContas);
		
		ContaAbstrata[] retorno = new ContaAbstrata[listaContas.size()];
		
		listaContas.toArray(retorno);
		return retorno;
	}

	@Override
	public Cliente[] listarClientesSaldoAcimaValor(double valor) {
		
		Set<Cliente> clientesVip = new HashSet<Cliente>();
		
		Collection<ContaAbstrata> contas = repositorio.values();
		for (ContaAbstrata conta : contas) {
			if(conta.getSaldo() >= valor) {
				clientesVip.add(conta.getCliente());
			}
		}
		Cliente[] clientesRetorno = new Cliente[clientesVip.size()];
		clientesVip.toArray(clientesRetorno);
		
		return clientesRetorno;
	}

	@Override
	public String imprimirExtrato(String numero) {
		
		ContaAbstrata conta = procurar(numero);
		if (conta != null) {
			List<Movimentacao> movimentacaos = conta.getMovimentacoes();
			Collections.sort(movimentacaos);
			
			String extrato = "Extrato da conta: " + numero + "\n\n";
			
			for (Movimentacao mov : movimentacaos) {
				if (mov != null) {
					extrato = extrato + mov.toString() + "\n\n**********************************************";
				}
			}
			return extrato;
		}
		return null;
	}
	
	

}
