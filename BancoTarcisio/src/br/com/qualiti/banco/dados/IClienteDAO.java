package br.com.qualiti.banco.dados;

import br.com.qualiti.banco.modelo.Cliente;

public interface IClienteDAO extends IGenericDAO<Cliente, String>{
	
	Cliente[] listarClientesOrdemCpf();

}
