package br.com.qualiti.banco.dados.jdbc;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;

import br.com.qualiti.banco.dados.IClienteDAO;
import br.com.qualiti.banco.excecoes.BancoException;
import br.com.qualiti.banco.modelo.Cliente;

public class ClienteDAOJDBCImp implements IClienteDAO{

	@Override
	public void inserir(Cliente entity) throws BancoException {
		
		Connection con = JDBCConnection.getConnection();
		
		try {
			
			Statement stm = con.createStatement();
			
			String sql = "INSERT INTO pessoa (nome, cpf, email, login, senha)" + 
					" VALUES('"+ 
						entity.getNome() +"','"+ entity.getCpf() +"','"+
						entity.getEmail() +"','"+ entity.getLogin() +"','"+ entity.getSenha() +"')";
			
			stm.executeUpdate(sql);
			
		} catch (SQLException e) {
			throw new BancoException("Problema ao acessar a base de dados");
		}
		finally {
			try {
				con.close();
			}
			catch (SQLException e) {
				throw new BancoException("Problemas ao fechar a conexao");
			}
		}
		
	}


	@Override
	public Cliente procurar(String id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void atualizar(Cliente entity) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void remover(String id) throws BancoException {
		
		Connection con = JDBCConnection.getConnection();
		
		try {
			
			String sql = "DELETE FROM pessoa where cpf = ?";
			
			PreparedStatement pstm = con.prepareStatement(sql);
			
			pstm.setString(1, id);
			
			pstm.executeUpdate(sql);
			
		} catch (SQLException e) {
			throw new BancoException("Problema ao acessar a base de dados");
		}
		finally {
			try {
				con.close();
			}
			catch (SQLException e) {
				throw new BancoException("Problemas ao fechar a conexao");
			}
		}

		
	}

	@Override
	public Cliente[] listarClientesOrdemCpf() {
		// TODO Auto-generated method stub
		return null;
	}

}
