package br.com.qualiti.banco.dados.jdbc;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

import br.com.qualiti.banco.dados.IContaDAO;
import br.com.qualiti.banco.excecoes.BancoException;
import br.com.qualiti.banco.modelo.Cliente;
import br.com.qualiti.banco.modelo.ContaAbstrata;
import br.com.qualiti.banco.modelo.ContaBonus;

public class ContaDAOJDBCImp implements IContaDAO{

	@Override
	public void inserir(ContaAbstrata entity) throws BancoException {
		
		Connection con = JDBCConnection.getConnection();
		
		try {
			
			Statement stm = con.createStatement();
			
			String sql = "INSERT INTO conta (numero, saldo, cpf, tipo) VALUES(' " + entity.getNumero() + ", " + entity.getSaldo() + ",' "
					+ entity.getCliente().getCpf() + " '," + entity.getTipo().name() + ")";
			
			if(entity instanceof ContaBonus) {
				ContaBonus cb = (ContaBonus)entity;
				sql = "INSERT INTO conta (numero, saldo, cpf, tipo, bonus) VALUES(' " + entity.getNumero() + " ', " + entity.getSaldo() + ",' "
					+ entity.getCliente().getCpf() + " '," + entity.getTipo().name() + "," + cb.getBonus() + ")";
			}
			
			stm.executeUpdate(sql);
			
		} catch (SQLException e) {
			throw new BancoException("Problema ao acessar a base de dados");
		}
		finally {
			try {
				con.close();
			}
			catch (SQLException e) {
				throw new BancoException("Problemas ao fechar a conexao");
			}
		}
		
	}

	@Override
	public ContaAbstrata procurar(String id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void atualizar(ContaAbstrata entity) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void remover(String id) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void renderJurosPoucancas(double taxa) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void renderBonusContaBonus() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public ContaAbstrata[] listarContasOrdemSaldo() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Cliente[] listarClientesSaldoAcimaValor(double valor) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String imprimirExtrato(String numero) {
		// TODO Auto-generated method stub
		return null;
	}

}
