package br.com.qualiti.banco.dados.jdbc;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class JDBCConnection {
	
	public static Connection getConnection() {
		try {
			Class.forName("com.mysql.jdbc.Driver");
			//jdbc:mysql://localhost:3306/banco
			//jdbc:postgresql://host:port/database
			String url = "jdbc:mysql://localhost:3306/banco";
			String user = "root";
			String pass = "123";
			return DriverManager.getConnection(url, user, pass);
		}
		catch (ClassNotFoundException | SQLException e) {
			return null;
		}
	}

	public static void main(String[] args) {
		Connection con = JDBCConnection.getConnection();
		if (con != null) {
			System.out.println("Conexao realizada");
		}
		else {
			System.out.println("Falha na conexao");
		}
	}
}
