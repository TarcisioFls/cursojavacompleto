package br.com.qualiti.banco.array;

import java.util.Arrays;

import br.com.qualiti.banco.dados.IClienteDAO;
import br.com.qualiti.banco.modelo.Cliente;

public class ClienteDAOArrayImp extends GenerericDAOArrayImp<Cliente, String> implements IClienteDAO {
	
	public Cliente[] listarClientesOrdemCpf() {
		Arrays.sort(repositorio);
		return repositorio;
	}

}
