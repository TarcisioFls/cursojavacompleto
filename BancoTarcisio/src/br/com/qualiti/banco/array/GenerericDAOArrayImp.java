package br.com.qualiti.banco.array;

import br.com.qualiti.banco.dados.IGenericDAO;
import br.com.qualiti.banco.excecoes.BancoException;
import br.com.qualiti.banco.modelo.AbstractEntify;

public class GenerericDAOArrayImp<T extends AbstractEntify<ID> , ID> implements IGenericDAO<T, ID> {

	protected T[] repositorio;
	protected int indice;
	
	@Override
	public void inserir(T entity) throws BancoException {
		if(indice < repositorio.length){
			repositorio[indice] = entity;
			indice++;
		}else{
			throw new BancoException("Limite de armazenamento atingido");
		}
		
	}

	@Override
	public T procurar(ID id) {
		for(int i = 0; i < indice; i++){
			if(repositorio[i] != null && 
					repositorio[i].getID().equals(id)){
				return repositorio[i];
			}
		}
		return null;
	}

	@Override
	public void atualizar(T entity) {
		for(int i = 0; i < indice; i++){
			if(repositorio[i] != null && 
					repositorio[i].getID().equals(entity.getID())){
				repositorio[i] = entity;
			}
		}
		
	}

	@Override
	public void remover(ID id) {
		for(int i = 0; i < indice; i++){
			if(repositorio[i] != null && 
					repositorio[i].getID().equals(id)){
				repositorio[i] = repositorio[indice - 1];
				repositorio[indice - 1] = null;
				indice--;
			}
		}
	}

}
