package br.com.qualiti.banco.array;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import br.com.qualiti.banco.dados.IContaDAO;
import br.com.qualiti.banco.modelo.Cliente;
import br.com.qualiti.banco.modelo.ContaAbstrata;
import br.com.qualiti.banco.modelo.ContaBonus;
import br.com.qualiti.banco.modelo.Movimentacao;
import br.com.qualiti.banco.modelo.Poupanca;

public class ContaDAOArrayImp extends GenerericDAOArrayImp<ContaAbstrata, String> implements IContaDAO {

	
	public void renderJurosPoucancas(double taxa){
		for(int i = 0; i < indice; i++){
			if(repositorio[i] != null && repositorio[i] instanceof Poupanca){
				ContaAbstrata conta = repositorio[i];
				Poupanca p = (Poupanca)conta;
				p.renderJuros(taxa);
			}
		}
	}
	
	public void renderBonusContaBonus(){
		for(int i = 0; i < indice; i++){
			if(repositorio[i] != null && repositorio[i] instanceof ContaBonus){
				ContaAbstrata conta = repositorio[i];
				ContaBonus cb = (ContaBonus)conta;
				cb.renderBonus();
			}
		}
	}
	
	public ContaAbstrata[] listarContasOrdemSaldo(){
		Arrays.sort(repositorio);
		return repositorio;
	}


	@Override
	public Cliente[] listarClientesSaldoAcimaValor(double valor) {
		if(indice == 0){
			return null;
		}
		Cliente[] clientes = new Cliente[indice];
		int indiceLocal = 0;
		for(ContaAbstrata conta : repositorio){
			if(conta != null && conta.getSaldo() >= valor){
				Cliente cliente = conta.getCliente();
				clientes[indiceLocal] = cliente;
				indiceLocal++;
			}
		}
		return clientes;
	}


	@Override
	public String imprimirExtrato(String numero) {
		ContaAbstrata conta = procurar(numero);
		if(conta != null){
			List<Movimentacao> movimentacoes = conta.getMovimentacoes();
			Collections.sort(movimentacoes);
			
			String extrato = "Extrato da conta: "+numero+"\n\n";
			
			for(Movimentacao mov : movimentacoes){
				if(mov != null){
					extrato = extrato + mov.toString() + 
							"\n\n*********************************************";
				}
			}
		
			return extrato;
		}
		return null;
	}
}
