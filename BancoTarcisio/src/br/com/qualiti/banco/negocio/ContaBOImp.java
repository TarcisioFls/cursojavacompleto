package br.com.qualiti.banco.negocio;

import br.com.qualiti.banco.dados.IContaDAO;
import br.com.qualiti.banco.excecoes.BancoException;
import br.com.qualiti.banco.modelo.Cliente;
import br.com.qualiti.banco.modelo.ContaAbstrata;

public class ContaBOImp implements IContaBO {
	
	private IContaDAO repositorio;
	
	public ContaBOImp(IContaDAO repositorio){
		this.repositorio = repositorio;
	}
	
	public void inserir(ContaAbstrata conta) throws BancoException {
		if(conta != null){
			if(conta.getNumero() != null && 
					conta.getNumero().trim().length() == 7){
				if(conta.getCliente() != null && conta.getCliente().getCpf() != null){
					if(conta.getSaldo() >= 500){
						ContaAbstrata retorno = repositorio.procurar(conta.getNumero());
						if(retorno == null){
							repositorio.inserir(conta);
						}else{
							throw new BancoException("Conta J� Existe");
						}
					}else{
						throw new BancoException("Saldo inicial menor que 500");
					}
				}else{
					throw new BancoException("Cliente invalido");
				}
			}else{
				throw new BancoException("Numero da conta invalido");
			}
		}else{
			throw new BancoException("Objeto Conta nulo");
		}
	}
	
	public ContaAbstrata procurar(String numero) throws BancoException {
		if(numero != null && numero.trim().length() == 7){
			return repositorio.procurar(numero);
		}else{
			throw new BancoException ("Numero da conta invalido");
		}
	}
	
	public void atualizar(ContaAbstrata conta) throws BancoException {
		if(conta != null){
			if(conta.getNumero() != null && 
					conta.getNumero().trim().length() == 7){
				if(conta.getCliente() != null && conta.getCliente().getCpf() != null){
					ContaAbstrata retorno = repositorio.procurar(conta.getNumero());
					if(retorno != null){
						repositorio.atualizar(conta);
					}else{
						throw new BancoException("Conta n�o Existe");
					}
				}else{
					throw new BancoException("Cliente invalido");
				}
			}else{
				throw new BancoException("Numero da conta invalido");
			}
		}else{
			throw new BancoException("Objeto Conta nulo");
		}
	}
	
	public void remover(String numero) throws BancoException {
		if(numero != null && numero.trim().length() == 7){
			ContaAbstrata conta = repositorio.procurar(numero);
			if(conta != null){
				repositorio.remover(numero);
			}else{
				throw new BancoException("Conta n�o existe");
			}
		}else{
			throw new BancoException("Numero da conta invalido");
		}
	}
	
	public void renderJurosPoucancas(double taxa) throws BancoException {
		if(taxa > 0){
			repositorio.renderJurosPoucancas(taxa);
		}else{
			throw new BancoException("taxa de juros invalida");
		}
	}
	
	public void renderBonusContaBonus(){
		repositorio.renderBonusContaBonus();
	}
	
	public ContaAbstrata[] listarContasOrdemSaldo(){
		return repositorio.listarContasOrdemSaldo();
	}

	@Override
	public Cliente[] listarClientesSaldoAcimaValor(double valor) 
														throws BancoException{
		if(valor > 0){
			return repositorio.listarClientesSaldoAcimaValor(valor);
		}else{
			throw new BancoException("Valor do saldo inv�lido");
		}
	}

	@Override
	public String imprimirExtrato(String numero) throws BancoException {
		if(numero != null && numero.length() == 7){
			return repositorio.imprimirExtrato(numero);
		}else{
			throw new BancoException("Numero da conta inv�lido");
		}
	}

}
