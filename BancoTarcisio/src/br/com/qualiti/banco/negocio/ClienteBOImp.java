package br.com.qualiti.banco.negocio;

import br.com.qualiti.banco.dados.IClienteDAO;
import br.com.qualiti.banco.excecoes.BancoException;
import br.com.qualiti.banco.modelo.Cliente;

public class ClienteBOImp implements IClienteBO {
	
	private IClienteDAO repositorio;
	
	public ClienteBOImp(IClienteDAO repositorio){
		this.repositorio = repositorio;
	}
	
	public void inserir(Cliente cliente) throws BancoException {
		if(cliente != null){
			if(cliente.getCpf() != null && cliente.getCpf().trim().length() == 14){
				if(cliente.getEndereco() != null){
					if(cliente.getNome() != null && 
							cliente.getNome().trim().length() > 0){
						if(cliente.getTelefone() != null && 
								cliente.getTelefone().trim().length() == 18){
							Cliente retorno = repositorio.procurar(cliente.getCpf());
							if(retorno == null){
								repositorio.inserir(cliente);
							}else{
								throw new BancoException("Cliente j� Existe");
							}
						}else{
							throw new BancoException("Telefone invalido");
						}
					}else{
						throw new BancoException("Nome invalido");
					}
				}else{
					throw new BancoException("Endereco invalido");
				}
			}else{
				throw new BancoException("CPF invalido");
			}
		}else{
			throw new BancoException("Objeto Cliente nulo");
		}
	}
	
	public Cliente procurar(String cpf) throws BancoException {
		if(cpf != null && cpf.trim().length() == 14){
			return repositorio.procurar(cpf);
		}else{
			throw new BancoException("CPF inv�lido");
		}
	}
	
	public void atualizar(Cliente cliente) throws BancoException{
		if(cliente != null){
			if(cliente.getCpf() != null && cliente.getCpf().trim().length() == 14){
				if(cliente.getEndereco() != null){
					if(cliente.getNome() != null && 
							cliente.getNome().trim().length() > 0){
						if(cliente.getTelefone() != null && 
								cliente.getTelefone().trim().length() == 18){
							Cliente retorno = repositorio.procurar(cliente.getCpf());
							if(retorno != null){
								repositorio.atualizar(cliente);
							}else{
								throw new BancoException("Cliente n�o Existe");
							}
						}else{
							throw new BancoException("Telefone invalido");
						}
					}else{
						throw new BancoException("Nome invalido");
					}
				}else{
					throw new BancoException("Endereco invalido");
				}
			}else{
				throw new BancoException("CPF invalido");
			}
		}else{
			throw new BancoException("Objeto Cliente nulo");
		}
	}
	
	public void remover(String cpf) throws BancoException{
		if(cpf != null && cpf.trim().length() == 14){
			Cliente cliente = repositorio.procurar(cpf);
			if(cliente != null){
				repositorio.remover(cpf);
			}else{
				throw new BancoException("Cliente n�o existe para remo��o");
			}
		}else{
			throw new BancoException("CPF inv�lido");
		}
	}
	
	public Cliente[] listarClientesOrdemCpf() {
		return repositorio.listarClientesOrdemCpf();
	}

}
