package br.com.qualiti.banco.negocio;

import br.com.qualiti.banco.excecoes.BancoException;
import br.com.qualiti.banco.modelo.Cliente;

public interface IClienteBO {
	
	void inserir(Cliente cliente) throws BancoException;
	Cliente procurar(String cpf) throws BancoException;
	void atualizar(Cliente cliente) throws BancoException;
	void remover(String cpf) throws BancoException;
	Cliente[] listarClientesOrdemCpf();

}
