package br.com.qualiti.banco.negocio;

import br.com.qualiti.banco.excecoes.BancoException;
import br.com.qualiti.banco.modelo.Cliente;
import br.com.qualiti.banco.modelo.ContaAbstrata;

public interface IContaBO {
	
	void inserir(ContaAbstrata conta) throws BancoException;
	ContaAbstrata procurar(String numero) throws BancoException;
	void atualizar(ContaAbstrata conta) throws BancoException;
	void remover(String numero) throws BancoException;
	void renderJurosPoucancas(double taxa) throws BancoException;
	void renderBonusContaBonus();
	ContaAbstrata[] listarContasOrdemSaldo();
	Cliente[] listarClientesSaldoAcimaValor(double valor) throws BancoException;
	public String imprimirExtrato(String numero) throws BancoException;
	
}
