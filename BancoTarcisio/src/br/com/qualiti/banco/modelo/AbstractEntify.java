package br.com.qualiti.banco.modelo;

public abstract class AbstractEntify<ID> {
	
	public abstract ID getID();

}
