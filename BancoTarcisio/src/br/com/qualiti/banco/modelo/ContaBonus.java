package br.com.qualiti.banco.modelo;

public class ContaBonus extends Conta {
	
	private double bonus;
	
	public void creditar(double valor){
		bonus = bonus + valor*0.1;
		super.creditar(valor);
	}
	
	public void renderBonus(){
		setSaldo(getSaldo() + bonus);
		bonus = 0;
	}

	public double getBonus() {
		return bonus;
	}

	public void setBonus(double bonus) {
		this.bonus = bonus;
	}

}
