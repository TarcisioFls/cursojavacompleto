package br.com.qualiti.banco.modelo;

public enum TipoMovimentacao {
	
	DEBITO,
	CREDITO,
	TRANSFERENCIA;

}
