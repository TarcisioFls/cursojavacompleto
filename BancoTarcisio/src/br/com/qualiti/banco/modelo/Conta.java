package br.com.qualiti.banco.modelo;

import java.util.Date;

import br.com.qualiti.banco.excecoes.BancoException;

public class Conta extends ContaAbstrata {

	@Override
	public void debitar(double valor) throws BancoException {
		if(getSaldo() >= valor){
			setSaldo(getSaldo() - valor);
			
			Movimentacao mov = new Movimentacao();
			mov.setConta(this);
			Date data = new Date();
			mov.setDataMovimentacao(data);
			mov.setTipo(TipoMovimentacao.DEBITO);
			mov.setValor(valor);
			
			adicionarMovimentacao(mov);
			
		}else{
			throw new BancoException("Saldo insuficiente");
		}
	}

	@Override
	public void debitarSemMovimentacao(double valor) throws BancoException {
		if(getSaldo() >= valor){
			setSaldo(getSaldo() - valor);
		}else{
			throw new BancoException("Saldo insuficiente");
		}
	}
	
}
