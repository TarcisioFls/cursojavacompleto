package br.com.qualiti.banco.modelo;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Movimentacao implements Comparable<Movimentacao>{
	
	private Date dataMovimentacao;
	private double valor;
	private ContaAbstrata conta;
	private String contaDestino;
	private TipoMovimentacao tipo;
	
	@Override
	public String toString(){
		DateFormat df = new SimpleDateFormat("dd/MM/yyyy HH:mm");
		String dataFormatada = df.format(dataMovimentacao);
		String movimentacao = "Tipo Movimentação: "+tipo.name()+"\n"+
				"Data: "+dataFormatada+"\n"+
				"Valor: "+valor+"\n";
		if(tipo.equals(TipoMovimentacao.TRANSFERENCIA)){
			if(contaDestino == null){
				movimentacao = movimentacao + "Conta Fonte: "+conta.getNumero();
			}else{
				movimentacao = movimentacao + "Conta Destino: "+contaDestino;
			}
		}
		
		return movimentacao;
	}
	
	public Date getDataMovimentacao() {
		return dataMovimentacao;
	}
	public void setDataMovimentacao(Date dataMovimentacao) {
		this.dataMovimentacao = dataMovimentacao;
	}
	public double getValor() {
		return valor;
	}
	public void setValor(double valor) {
		this.valor = valor;
	}
	public ContaAbstrata getConta() {
		return conta;
	}
	public void setConta(ContaAbstrata conta) {
		this.conta = conta;
	}
	public String getContaDestino() {
		return contaDestino;
	}
	public void setContaDestino(String contaDestino) {
		this.contaDestino = contaDestino;
	}
	public TipoMovimentacao getTipo() {
		return tipo;
	}
	public void setTipo(TipoMovimentacao tipo) {
		this.tipo = tipo;
	}
	@Override
	public int compareTo(Movimentacao arg0) {
		return dataMovimentacao.compareTo(arg0.getDataMovimentacao());
	}
	
	
}
