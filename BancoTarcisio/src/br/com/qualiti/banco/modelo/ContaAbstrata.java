package br.com.qualiti.banco.modelo;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import br.com.qualiti.banco.excecoes.BancoException;

public abstract class ContaAbstrata extends AbstractEntify<String> implements Comparable<ContaAbstrata> {
	
	private String numero;
	private String agencia;
	private TipoConta tipo;
	private double saldo;
	private Cliente cliente;
	private List<Movimentacao> movimentacoes;
	
	public ContaAbstrata(String numero){
		this.numero = numero;
		movimentacoes = new ArrayList<Movimentacao>();
	}
	
	public ContaAbstrata(){
		movimentacoes = new ArrayList<Movimentacao>();
	}
	
	@Override
	public int compareTo(ContaAbstrata arg0) {
		if(saldo == arg0.getSaldo()){
			return 0;
		}else if(saldo < arg0.getSaldo()){
			return -1;
		}else{
			return 1;
		}
	}

	public void creditar(double valor){
		saldo = saldo + valor;
		
		Movimentacao mov = new Movimentacao();
		mov.setConta(this);
		Date data = new Date();
		mov.setDataMovimentacao(data);
		mov.setTipo(TipoMovimentacao.CREDITO);
		mov.setValor(valor);
		
		this.adicionarMovimentacao(mov);
	}
	
	public void creditarSemMovimentacao(double valor){
		saldo = saldo + valor;
	}
	
	public void adicionarMovimentacao(Movimentacao movimentacao){
		movimentacoes.add(movimentacao);
	}
	
	public abstract void debitar(double valor) throws BancoException;
	public abstract void debitarSemMovimentacao(double valor) throws BancoException;
	
	public void transferir(ContaAbstrata contaDestino, double valor)
															throws BancoException{
		debitarSemMovimentacao(valor);
		contaDestino.creditarSemMovimentacao(valor);
		
		Movimentacao mov = new Movimentacao();
		mov.setConta(this);
		Date data = new Date();
		mov.setDataMovimentacao(data);
		mov.setTipo(TipoMovimentacao.TRANSFERENCIA);
		mov.setValor(valor);
		mov.setContaDestino(contaDestino.getNumero());
		
		adicionarMovimentacao(mov);
		
		Movimentacao movContaDestino = new Movimentacao();
		movContaDestino.setConta(contaDestino);
		movContaDestino.setDataMovimentacao(data);
		movContaDestino.setTipo(TipoMovimentacao.TRANSFERENCIA);
		movContaDestino.setValor(valor);
		
		contaDestino.adicionarMovimentacao(movContaDestino);
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((numero == null) ? 0 : numero.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ContaAbstrata other = (ContaAbstrata) obj;
		if (numero == null) {
			if (other.numero != null)
				return false;
		} else if (!numero.equals(other.numero))
			return false;
		return true;
	}

	public String getNumero() {
		return numero;
	}
	public void setNumero(String numero) {
		this.numero = numero;
	}
	public String getAgencia() {
		return agencia;
	}
	public void setAgencia(String agencia) {
		this.agencia = agencia;
	}
	public TipoConta getTipo() {
		return tipo;
	}
	public void setTipo(TipoConta tipo) {
		this.tipo = tipo;
	}
	public double getSaldo() {
		return saldo;
	}
	public void setSaldo(double saldo) {
		this.saldo = saldo;
	}
	public Cliente getCliente() {
		return cliente;
	}
	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}

	public List<Movimentacao> getMovimentacoes() {
		return movimentacoes;
	}

	public void setMovimentacoes(List<Movimentacao> movimentacoes) {
		this.movimentacoes = movimentacoes;
	}
	
	@Override
	public String getID() {
		return numero;
	}

}
