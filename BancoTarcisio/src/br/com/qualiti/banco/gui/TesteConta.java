package br.com.qualiti.banco.gui;

import br.com.qualiti.banco.excecoes.BancoException;
import br.com.qualiti.banco.fachada.Fachada;
import br.com.qualiti.banco.modelo.Cliente;
import br.com.qualiti.banco.modelo.Conta;
import br.com.qualiti.banco.modelo.ContaAbstrata;

public class TesteConta {

	/**
	 * @param args
	 */
	public static void main(String[] args){
		
		Fachada fachada = Fachada.getInstance();
		Cliente cli = new Cliente();
		cli.setCpf("45124545454545");
		cli.setNome("AAAAAAAAAAAAAAAAA");
		
		Conta conta = new Conta();
		conta.setCliente(cli);
		conta.setNumero("1234567");
		conta.setSaldo(500);
		
		Conta conta2 = new Conta();
		conta2.setCliente(cli);
		conta2.setNumero("1234568");
		conta2.setSaldo(500);
		
		try {
			fachada.inserirConta(conta);
			fachada.inserirConta(conta2);
			
			ContaAbstrata c1 = fachada.procurarConta("1234567");
			ContaAbstrata c2 = fachada.procurarConta("1234568");
			
			c1.creditar(600);
			c1.debitar(200);
			c1.transferir(c2, 100);
			
			c2.creditar(400);
			c2.creditar(800);
			
			System.out.println(fachada.imprimirExtrato("1234567"));
			System.out.println(fachada.imprimirExtrato("1234568"));
			
		} catch (BancoException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		
		
	}
}
