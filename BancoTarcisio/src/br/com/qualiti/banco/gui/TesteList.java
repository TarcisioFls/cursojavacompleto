package br.com.qualiti.banco.gui;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class TesteList {
	
	public static void main(String[] args) {
		
		List<String> nomes = new ArrayList<String>();
		nomes.add("Tarcisio");
		nomes.add("Francisco");
		nomes.add("Lima");
		nomes.add("da");
		nomes.add("Silva");
		nomes.add("tarcisio");
		nomes.add("francisco");
		
		Iterator<String> it = nomes.iterator();
		
		while(it.hasNext()) {
			String nome = it.next();
			if (nome.equals("tarcisio")) {
				it.remove();
			}
		}
		
//		for(String nome : nomes) {
//			if(nome.equals("tarcisio")) {
//				nomes.remove("tarcisio");
//			}
//		}
		
		System.out.println(nomes);
		
	}

}
