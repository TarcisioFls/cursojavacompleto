package br.com.qualiti.banco.gui;

import br.com.qualiti.banco.excecoes.BancoException;
import br.com.qualiti.banco.fachada.Fachada;
import br.com.qualiti.banco.modelo.Cliente;
import br.com.qualiti.banco.modelo.Conta;
import br.com.qualiti.banco.modelo.Endereco;
import br.com.qualiti.banco.modelo.TipoConta;

public class TesteJDBC {
	
	public static void main(String[] args) {
		
		Cliente cliente = new Cliente();
		cliente.setCpf("12345678945124");
		cliente.setEmail("tarcisio@hotmail.com");
		cliente.setLogin("tarcisio");
		cliente.setNome("Tarcisio");
		cliente.setSenha("123");
		cliente.setEndereco(new  Endereco());
		cliente.setTelefone("999999999999999999");
		
		Conta conta = new Conta();
		conta.setCliente(cliente);
		conta.setNumero("8888888");
		conta.setSaldo(2500);
		conta.setTipo(TipoConta.CORRENTE);
		
		Fachada fachada = Fachada.getInstance();
		try {
			fachada.inserirCliente(cliente);
			fachada.inserirConta(conta);
		} catch (BancoException e) {
			e.printStackTrace();
		}
		
		
	}

}
