package br.com.qualiti.banco.gui;

import javax.swing.JPanel;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JList;
import javax.swing.JComboBox;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class AdicionarClientePanel extends JPanel {
	private JTextField textField;
	private JTextField textField_1;
	private JTextField textField_2;
	private JTextField textField_3;
	private JTextField textField_4;
	private JTextField textField_5;
	private JTextField textField_6;
	private JTextField textField_7;
	private JTextField textField_8;
	private JTextField textField_9;
	private JTextField textField_10;

	/**
	 * Create the panel.
	 */
	public AdicionarClientePanel() {
		setLayout(null);
		
		JLabel label = new JLabel("CPF:");
		label.setBounds(12, 31, 70, 15);
		add(label);
		
		textField = new JTextField();
		textField.setBounds(66, 29, 152, 19);
		add(textField);
		textField.setColumns(10);
		
		JLabel lblNewLabel = new JLabel("Nome:");
		lblNewLabel.setBounds(12, 58, 70, 15);
		add(lblNewLabel);
		
		textField_1 = new JTextField();
		textField_1.setBounds(66, 56, 345, 19);
		add(textField_1);
		textField_1.setColumns(10);
		
		JLabel lblNewLabel_1 = new JLabel("E-mail:");
		lblNewLabel_1.setBounds(12, 85, 70, 15);
		add(lblNewLabel_1);
		
		textField_2 = new JTextField();
		textField_2.setBounds(65, 85, 346, 19);
		add(textField_2);
		textField_2.setColumns(10);
		
		JLabel lblNewLadddddbel = new JLabel("Login:");
		lblNewLadddddbel.setBounds(12, 112, 70, 15);
		add(lblNewLadddddbel);
		
		textField_3 = new JTextField();
		textField_3.setBounds(66, 112, 152, 19);
		add(textField_3);
		textField_3.setColumns(10);
		
		JLabel lblNewLabel_2 = new JLabel("Senha:");
		lblNewLabel_2.setBounds(223, 112, 70, 15);
		add(lblNewLabel_2);
		
		textField_4 = new JTextField();
		textField_4.setBounds(275, 110, 136, 19);
		add(textField_4);
		textField_4.setColumns(10);
		
		JLabel lblLogradouro = new JLabel("Logradouro:");
		lblLogradouro.setBounds(12, 157, 90, 15);
		add(lblLogradouro);
		
		textField_5 = new JTextField();
		textField_5.setBounds(114, 155, 324, 19);
		add(textField_5);
		textField_5.setColumns(10);
		
		JLabel lblNmero = new JLabel("Número:");
		lblNmero.setBounds(12, 184, 70, 15);
		add(lblNmero);
		
		textField_6 = new JTextField();
		textField_6.setBounds(114, 184, 126, 19);
		add(textField_6);
		textField_6.setColumns(10);
		
		JLabel lblCep = new JLabel("CEP:");
		lblCep.setBounds(253, 186, 39, 15);
		add(lblCep);
		
		textField_7 = new JTextField();
		textField_7.setBounds(297, 186, 105, 19);
		add(textField_7);
		textField_7.setColumns(10);
		
		JLabel lblComplemento = new JLabel("Complemento:");
		lblComplemento.setBounds(12, 218, 108, 15);
		add(lblComplemento);
		
		textField_8 = new JTextField();
		textField_8.setBounds(114, 216, 114, 19);
		add(textField_8);
		textField_8.setColumns(10);
		
		JLabel lblBairro = new JLabel("Bairro:");
		lblBairro.setBounds(242, 218, 48, 15);
		add(lblBairro);
		
		textField_9 = new JTextField();
		textField_9.setBounds(297, 216, 114, 19);
		add(textField_9);
		textField_9.setColumns(10);
		
		JLabel lblCidade = new JLabel("Cidade:");
		lblCidade.setBounds(12, 245, 70, 15);
		add(lblCidade);
		
		textField_10 = new JTextField();
		textField_10.setBounds(114, 245, 114, 19);
		add(textField_10);
		textField_10.setColumns(10);
		
		JLabel lblNewLabel_3 = new JLabel("UF:");
		lblNewLabel_3.setBounds(242, 245, 28, 15);
		add(lblNewLabel_3);
		
		JComboBox comboBox = new JComboBox();
		comboBox.setBounds(272, 240, 48, 24);
		add(comboBox);
		
		JButton btnNewButton = new JButton("Salvar");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			}
		});
		btnNewButton.setBounds(36, 303, 117, 25);
		add(btnNewButton);

	}
}
