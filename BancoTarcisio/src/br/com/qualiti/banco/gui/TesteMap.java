package br.com.qualiti.banco.gui;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class TesteMap {
	
	public static void main(String[] args) {
		
		Map<String, String> mapNomes = new HashMap<String, String>();
		
		mapNomes.put("088", "Maria");
		mapNomes.put("077", "Joao");
		
		
		String nome = mapNomes.get("088");
		
		System.out.println(nome);
		
		Set<String> keys = mapNomes.keySet();
		System.out.println(keys);
		
		Collection<String> nomes = mapNomes.values();
		System.out.println(nomes);
		
		mapNomes.remove("077");
		
		mapNomes.put("088", "jose");
		
	}

}
