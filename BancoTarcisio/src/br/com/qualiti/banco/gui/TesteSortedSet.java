package br.com.qualiti.banco.gui;

import java.util.ArrayList;
import java.util.List;

public class TesteSortedSet {
	
	public static void main(String[] args) {
		
		List<String> nomes = new ArrayList<String>();
		nomes.add("Tarcisio");
		nomes.add("Francisco");
		nomes.add("Lima");
		nomes.add("da");
		nomes.add("Silva");
		nomes.add("tarcisio");
		nomes.add("francisco");
		
		System.out.println(nomes);
		
	}

}
