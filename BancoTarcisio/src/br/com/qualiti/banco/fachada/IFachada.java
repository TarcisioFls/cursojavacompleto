package br.com.qualiti.banco.fachada;

import br.com.qualiti.banco.excecoes.BancoException;
import br.com.qualiti.banco.modelo.Cliente;
import br.com.qualiti.banco.modelo.ContaAbstrata;

public interface IFachada {
	
	void inserirConta(ContaAbstrata conta) throws BancoException ;
	ContaAbstrata procurarConta(String numero) throws BancoException;
	void atualizarConta(ContaAbstrata conta) throws BancoException;
	void removerConta(String numero) throws BancoException;
	void renderJurosPoucancas(double taxa) throws BancoException;
	void renderBonusContaBonus();
	ContaAbstrata[] listarContasOrdemSaldo();
	Cliente[] listarClientesSaldoAcimaValor(double valor) throws BancoException;
    String imprimirExtrato(String numero) throws BancoException;
	
	void inserirCliente(Cliente cliente) throws BancoException;
	Cliente procurarCliente(String cpf) throws BancoException;
	void atualizarCliente(Cliente cliente) throws BancoException;
	void removerCliente(String cpf) throws BancoException;
	Cliente[] listarClientesOrdemCpf();

}
