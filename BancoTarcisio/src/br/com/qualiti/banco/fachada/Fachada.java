package br.com.qualiti.banco.fachada;

import br.com.qualiti.banco.dados.IClienteDAO;
import br.com.qualiti.banco.dados.IContaDAO;
import br.com.qualiti.banco.dados.jdbc.ClienteDAOJDBCImp;
import br.com.qualiti.banco.dados.jdbc.ContaDAOJDBCImp;
import br.com.qualiti.banco.excecoes.BancoException;
import br.com.qualiti.banco.modelo.Cliente;
import br.com.qualiti.banco.modelo.ContaAbstrata;
import br.com.qualiti.banco.negocio.ClienteBOImp;
import br.com.qualiti.banco.negocio.ContaBOImp;
import br.com.qualiti.banco.negocio.IClienteBO;
import br.com.qualiti.banco.negocio.IContaBO;



public class Fachada implements IFachada {
	
	private IContaBO contaBO;
	private IClienteBO clienteBO;
	private static Fachada fachada;
	
	public static Fachada getInstance(){
		if(fachada == null){
			fachada = new Fachada();
		}
		return fachada;
	}
	
	private Fachada(){
		IContaDAO repositorioContas = new ContaDAOJDBCImp();
		contaBO = new ContaBOImp(repositorioContas);
		
		IClienteDAO repositorioClientes = new ClienteDAOJDBCImp();
		clienteBO = new ClienteBOImp(repositorioClientes);
	}

	@Override
	public void inserirConta(ContaAbstrata conta) throws BancoException {
		contaBO.inserir(conta);
	}

	@Override
	public ContaAbstrata procurarConta(String numero) throws BancoException {
		return contaBO.procurar(numero);
	}

	@Override
	public void atualizarConta(ContaAbstrata conta) throws BancoException {
		contaBO.atualizar(conta);	
	}

	@Override
	public void removerConta(String numero) throws BancoException{
		contaBO.remover(numero);
	}

	@Override
	public void renderJurosPoucancas(double taxa) throws BancoException {
		contaBO.renderJurosPoucancas(taxa);
	}

	@Override
	public void renderBonusContaBonus() {
		contaBO.renderBonusContaBonus();
	}

	@Override
	public ContaAbstrata[] listarContasOrdemSaldo() {
		return contaBO.listarContasOrdemSaldo();
	}

	@Override
	public void inserirCliente(Cliente cliente) throws BancoException {
		clienteBO.inserir(cliente);
	}

	@Override
	public Cliente procurarCliente(String cpf) throws BancoException {
		return clienteBO.procurar(cpf);
	}

	@Override
	public void atualizarCliente(Cliente cliente) throws BancoException{
		clienteBO.atualizar(cliente);
	}

	@Override
	public void removerCliente(String cpf) throws BancoException{
		clienteBO.remover(cpf);
	}

	@Override
	public Cliente[] listarClientesOrdemCpf() {
		return clienteBO.listarClientesOrdemCpf();
	}

	@Override
	public Cliente[] listarClientesSaldoAcimaValor(double valor) throws BancoException{
		return contaBO.listarClientesSaldoAcimaValor(valor);
	}

	@Override
	public String imprimirExtrato(String numero) throws BancoException {
		return contaBO.imprimirExtrato(numero);
	}

}
