package br.com.qualiti.banco.excecoes;

public class BancoException extends Exception {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 964119732776737456L;

	public BancoException(String mensagem){
		super(mensagem);
	}

}
